//
// Created by Matthew McKenie on 2016-07-03.
//

#include "math.h"
#include <math.h>


int ilog2(int v)
{
    int ret_val;

    ret_val = (int)log2(v);

    return ret_val;
}
