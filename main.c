#include "input.h"
#include "math.h"
#include "arrays.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>


void show_choices(int first, int second, char* names[]);
int make_choice(int first, int second, char* names[], int virtues[]);
void find_equals(int goal, int virtues[], int size, int matches[], int offset);
int select_class(char* names[], int virtues[], int virtues_count);

int main()
{
    char* names[] =
    {
        "honesty",
        "valour",
        "compassion",
        "justice",
        "sacrifice",
        "honour",
        "spirituality",
        "humility",
    };
    int* virtues;
    int virtues_count;
    int class;

    virtues_count = ARRAY_SIZE(names);
    virtues       = malloc(virtues_count * sizeof(int));

    for (int i = 0; i < virtues_count; i++)
    {
       virtues[i] = 0;
    }

    class = select_class(names, virtues, virtues_count);
    free(virtues);

    printf("%s\n", names[class]);

    return EXIT_SUCCESS;
}

void show_choices(int first, int second, char* names[])
{
    printf("%s - %s\n", names[first], names[second]);
}

int make_choice(int first, int second, char* names[], int virtues[])
{
    int choice;
    int virtue;

    show_choices(first, second, names);
    choice = choose_number(1, 2);

    if (choice == 1)
    {
        virtue = first;
    }
    else
    {
        virtue = second;
    }

    virtues[virtue]++;

    return virtue;
}

void find_equals(int goal, int virtues[], int size, int matches[], int offset)
{
    for (int i = 0; i < size; i++)
    {
        if (virtues[i] == goal)
        {
            matches[offset] = i;
            offset++;
        }
    }
}

int select_class(char* names[], int virtues[], int virtues_count)
{
    int* choices;
    int questions;
    int offset;
    int chosen_virtue;
    int number_of_rounds;
    int number_of_choices;

    number_of_choices = virtues_count + (virtues_count - 1);
    choices = malloc(number_of_choices * sizeof(int));

    for (int i = 0; i < number_of_choices; i++)
    {
        choices[i] = INT_MAX;
    }

    for (int i = 0; i < virtues_count; i++)
    {
        choices[i] = i;
    }

    questions        = virtues_count;
    offset           = 0;
    number_of_rounds = ilog2(virtues_count);

    for (int round = 0; round < number_of_rounds; round++)
    {
        for (int j = offset; j < offset + questions; j += 2)
        {
            make_choice(choices[j], choices[j + 1], names, virtues);
        }

        questions /= 2;
        offset += questions;
        find_equals(round + 1, virtues, virtues_count, choices, offset);
    }

    chosen_virtue = choices[virtues_count - 1];

    free(choices);

    return chosen_virtue;
}

