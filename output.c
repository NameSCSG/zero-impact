//
// Created by Matthew McKenie on 2016-06-30.
//
#include <stdio.h>
#include "output.h"

void print_int_array(int array[], int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("[%d] = %d\n", i, array[i]);
    }
}
