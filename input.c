#include "input.h"
#include <stdio.h>


int get_number(char *msg)
{
    int number;

    printf("%s", msg);
    scanf("%d", &number);

    return number;
}

int choose_number(int a, int b)
{

    int answer;

    do
    {
        printf("choose between %d or %d: ", a, b);
        scanf("%d", &answer);
    }
    while (answer != a && answer != b);

    return answer;
}
